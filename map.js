// Code largely from Leaflet tutorial at https://leafletjs.com/examples/quick-start/ and Leaflet.heat at
// https://github.com/Leaflet/Leaflet.heat

function getPoints(sw_corner, ne_corner) {
    var request = new XMLHttpRequest();
    // URL will need to be the actual URL instead of localhost when running on a server
    var url = `http://localhost:8000/api/getPoints?min-lat=${sw_corner.lat}&min-long=${sw_corner.lng}&max-lat=${ne_corner.lat}&max-long=${ne_corner.lng}`;
    request.open('GET', url, true);
    request.onload = function () {
      return JSON.parse(this.response).data;
      }
    request.send();
}

var mymap = L.map('mapid').setView([35.875233, -78.840692], 13);
L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1IjoiZmxvd2VybmNzdSIsImEiOiJjanhmMzcxbG4wZnhjM3NveXFkMHY5Ym1vIn0.oxspkHQvpfJiVWm3IXzw1A'
}).addTo(mymap);


var addressPoints = getPoints(mymap.getBounds().getSouthWest(), mymap.getBounds().getNorthEast());
console.log(addressPoints);
addressPoints = addressPoints.map(function (p) { return [p[0], p[1]]; });

var heat = L.heatLayer(addressPoints).addTo(map);