from django.db import models


class Location(models.Model):
    # 5 decimal places in a lat/long refines location to within a few feet
    # Latitudes range from -90 to 90, so up to 2 digits before the decimal
    latitude = models.DecimalField(max_digits=7, decimal_places=5)
    # Longitudes range from -180 to 180, so up to 3 digits before the decimal
    longitude = models.DecimalField(max_digits=8, decimal_places=5)