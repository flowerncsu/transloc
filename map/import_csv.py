import argparse, csv, django
from django.conf import settings
from .models import Location

settings.configure()
django.setup()

LAT_COL_NAME = 'latitude'
LONG_COL_NAME = 'longitude'


def load_from_csv(filename):
    # Returns empty string if successful, else error message
    # (or traceback; some errors intentionally not explicitly handled)
    with open(filename) as csvFile:
        lines = csv.reader(csvFile)
        header_row = next(lines)
        if all(header in header_row for header in [LAT_COL_NAME, LONG_COL_NAME]):
            latitude_column = header_row.index(LAT_COL_NAME)
            longitude_column = header_row.index(LONG_COL_NAME)
            locations = [(row[latitude_column], row[longitude_column]) for row in lines]
            Location.objects.bulk_create(
                [Location(latitude=latitude, longitude=longitude) for (latitude, longitude) in locations]
            )
            return ''
        else:
            return "File does not contain both 'latitude' and 'longitude' columns"


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="""
    Import a csv containing latitude and longitude data
    Should contain a header row identifying a latitude column ({LAT_COL_NAME}) and a longitude column ({LONG_COL_NAME})
    """.format(LAT_COL_NAME=LAT_COL_NAME, LONG_COL_NAME=LONG_COL_NAME))
    parser.add_argument('filename', type=str, help='Path to the csv to import')
    args = parser.parse_args()
    load_from_csv(args['filename'])
