from django.views.decorators.http import require_safe
from django.http import HttpResponseBadRequest, JsonResponse
from .models import Location


@require_safe
def getPoints(request):
    # Make sure we have the information we need to define the bounding box
    # TODO: validate that minimum lat/long is less than maximum lat/long
    # TODO: add handling for the case when bounding box spans the 180th meridian
    if all(param in request.GET.keys() for param in ['min-lat', 'min-long', 'max-lat', 'max-long']):
        points = Location.objects.filter(
            latitude__lte=request.GET['max-lat'],
            latitude__gte=request.GET['min-lat'],
            longitude__lte=request.GET['max-long'],
            longitude__gte=request.GET['min-long'],
        )
        return JsonResponse({'data': [
            [point.latitude, point.longitude, 1] for point in points]})
    else:
        return HttpResponseBadRequest("Request must include all parameters: min-lat, min-long, max-lat, max-long")
